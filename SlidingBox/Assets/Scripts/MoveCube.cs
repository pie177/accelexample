﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveCube : MonoBehaviour {

    float maxX;

    public AccelControl accel;

    public int moveSpeed;

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {

        maxX = Camera.main.orthographicSize * Camera.main.aspect;

        //Vector3 temp = Vector3.zero;
        //temp.x = accel.smoothAcceleration.x;
        //temp.x *= maxX;
        //transform.position = temp;


        Vector3 offset = Vector3.zero;

        offset.x = moveSpeed * accel.smoothAcceleration.x * Time.deltaTime;
        transform.position += offset;

        if(transform.position.x >= maxX)
        {
            Vector3 temp = transform.position;
            temp.x = maxX;
            transform.position = temp;
        }
        else if(transform.position.x <= -maxX)
        {
            Vector3 temp = transform.position;
            temp.x = -maxX;
            transform.position = temp;
        }
	}
}
