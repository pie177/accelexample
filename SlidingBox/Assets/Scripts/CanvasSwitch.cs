﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasSwitch : MonoBehaviour {

    [SerializeField]
    GameObject landscapeCanvas;

    [SerializeField]
    GameObject portraitCanvas;

	// Use this for initialization
	void Start ()
    {
		if(landscapeCanvas == null || portraitCanvas == null)
        {
            Debug.LogError("One or both canvases are not linked");
        }

        landscapeCanvas.SetActive(false);
        portraitCanvas.SetActive(true);
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
        {
            landscapeCanvas.SetActive(true);
            portraitCanvas.SetActive(false);
        }
        else if(Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
        {
            landscapeCanvas.SetActive(false);
            portraitCanvas.SetActive(true);
        }
	}
}
