﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreHolder : MonoBehaviour {

    public static ScoreHolder instance;

    public Text highScore;
    public Text scoreText;
    public int score;

    public GameObject memeBomb;
    public int bombStorage;
    [HideInInspector]
    public Vector3 startPoint = new Vector3(0, 0, -20);

    [HideInInspector]
    public List<GameObject> memeBombs = new List<GameObject>();

    public GameObject dropper;
    public GameObject canvas;

    [HideInInspector]
    public bool stopped;

    [HideInInspector]
    public int lives;

    public GameObject[] player;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        lives = 2;
        score = 0;
        highScore.text = "MaxFollowers: " + PlayerPrefs.GetInt("HighScore").ToString();
        stopped = true;

        for (int i = 0; i < bombStorage; i++)
        {
            GameObject tempObj = Instantiate(memeBomb, startPoint, Quaternion.identity);
            memeBombs.Add(tempObj);
            tempObj.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        scoreText.text = "Score: " + score;

        if(lives < 0)
        {
            stopped = true;
            PlayerPrefs.SetInt("HighScore", score);

            canvas.GetComponent<ButtonControl>().gameUI.SetActive(false);
            canvas.GetComponent<ButtonControl>().parent.SetActive(true);
            dropper.GetComponent<AI_Meme_Dropper>().dropSpeed = 1;
            dropper.GetComponent<AI_Meme_Dropper>().speed = 5;
        }

        if(score % 10 == 0 && lives < 2)
        {
            player[lives].SetActive(true);
        }
	}

    public void UpdateScore()
    {
        score++;
    }

    public void SetHighScore()
    {
        if(PlayerPrefs.GetInt("HighScore") < score)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
    }

    public void _Quit()
    {
        SetHighScore();
        Application.Quit();
    }

    public void BombDestroyer()
    {
        stopped = true;
        player[lives].SetActive(false);
        lives--;
        StartCoroutine(BombSweep());
    }

    IEnumerator BombSweep()
    {
        for(int i = 0; i < ScoreHolder.instance.memeBombs.Count; i++)
        {
            if(memeBombs[i].activeSelf == true)
            {
                memeBombs[i].transform.position = startPoint;
                memeBombs[i].SetActive(false);
                yield return new WaitForSeconds(1);
            } 
        }
        stopped = false;
        dropper.GetComponent<AI_Meme_Dropper>().StartRepeates();
    }
}
