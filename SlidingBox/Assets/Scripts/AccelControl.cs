﻿using UnityEngine;

public class AccelControl : MonoBehaviour {

    Vector3[] accelValues;
    public int maxValues = 5;

    public Vector3 smoothAcceleration;

	// Use this for initialization
	void Start ()
    {
        accelValues = new Vector3[maxValues];
        for(int i = 0; i < maxValues; i++)
        {
            accelValues[i] = Vector3.zero;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 temp = Vector3.zero;

        for(int i = 0; i < (maxValues - 1); i++)
        {
            accelValues[i] = accelValues[i + 1];
            temp += accelValues[i];
        }

        accelValues[maxValues - 1] = Input.acceleration;

        temp += accelValues[maxValues - 1];

        temp = temp / maxValues;

        smoothAcceleration = temp;
	}
}
