﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemeBomb : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!ScoreHolder.instance.stopped)
        {
            transform.Translate(Vector3.down * Time.deltaTime * ScoreHolder.instance.dropper.GetComponent<AI_Meme_Dropper>().speed);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            ScoreHolder.instance.score++;
            transform.position = ScoreHolder.instance.startPoint;
            gameObject.SetActive(false);
        }

        if(other.CompareTag("DeadZone"))
        {
            transform.position = ScoreHolder.instance.startPoint;
            ScoreHolder.instance.BombDestroyer();
            gameObject.SetActive(false);
        }
    }
}
