﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonControl : MonoBehaviour {

    public Text score;
    public GameObject parent;
    public GameObject gameUI;

    public GameObject dropper;

	// Use this for initialization
	void Start ()
    {
        score.text = "HighScore: " + PlayerPrefs.GetInt("HighScore");
	}
	
	// Update is called once per frame
	void Update ()
    {
        score.text = "HighScore: " + PlayerPrefs.GetInt("HighScore");
    }

    public void _Quit()
    {
        Application.Quit();
    }

    public void _Play()
    {
        ScoreHolder.instance.player[0].SetActive(true);
        ScoreHolder.instance.player[1].SetActive(true);
        ScoreHolder.instance.player[2].SetActive(true);
        ScoreHolder.instance.lives = 2;
        gameUI.SetActive(true);
        dropper.GetComponent<AI_Meme_Dropper>().StartRepeates();
        parent.SetActive(false);
        ScoreHolder.instance.stopped = false;
    }
}
