﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Meme_Dropper : MonoBehaviour {

    public int speed;
    [HideInInspector]
    public bool isStopped = false;

    float maxX;
    bool forward;
    bool backward;
    int counter = 0;
    public float dropSpeed = 1;

    // Use this for initialization
    void Start()
    {
        maxX = Camera.main.orthographicSize * Camera.main.aspect;
        forward = true;
        backward = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!ScoreHolder.instance.stopped)
        {
            if (forward)
            {
                transform.Translate(new Vector3(1, 0, 0) * speed * Time.deltaTime);
                if (transform.position.x >= maxX - .05)
                {
                    forward = false;
                    backward = true;
                }
            }
            else if (backward)
            {
                transform.Translate(new Vector3(-1, 0, 0) * speed * Time.deltaTime);
                if (transform.position.x <= -maxX + .05)
                {
                    forward = true;
                    backward = false;
                }
            }
        }
        else if(ScoreHolder.instance.stopped)
        {
            CancelInvoke("BombDropper");
            CancelInvoke("IncreaseSpeed");
            CancelInvoke("ReverseDirection");
        }
    }

    void ReverseDirection()
    {
        int temp = Random.Range(1, 6);
        if(temp == 3)
        {
            forward = !forward;
            backward = !backward;
        }
    }

    void BombDropper()
    {
        ScoreHolder.instance.memeBombs[counter].transform.position = new Vector3(transform.position.x, 7, transform.position.z);
        ScoreHolder.instance.memeBombs[counter].SetActive(true);
        counter++;
    }

    void IncreaseSpeed()
    {
        speed += 2;
        dropSpeed -= .2f;
    }

    public void StartRepeates()
    {
        InvokeRepeating("BombDropper", 0f, dropSpeed);
        InvokeRepeating("ReverseDirection", 0f, 2f);
        InvokeRepeating("IncreaseSpeed", 0f, 5f);
    }
}
